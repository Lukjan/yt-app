import { YoutubeApiService } from '../services/youtube-api.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements AfterViewInit, OnDestroy {
  private player: YT.Player;

  constructor(
    private route: ActivatedRoute,
    private youtubeApiService: YoutubeApiService
  ) { }

  ngAfterViewInit() {
    this.youtubeApiService.isYoutubePlayerLoaded$.subscribe((isLoaded) => {
      if (isLoaded) {
        this.route.queryParams.subscribe(params => {
          if (params.id) {
            this.onYouTubeIframeAPIReady(params.id);
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.player.destroy();
  }

  private onYouTubeIframeAPIReady(videoId): void {
    this.player = new YT.Player('player', {
      height: '360',
      width: '640',
      videoId,
      events: {
        onReady: this.onPlayerReady
      }
    });
  }

  private onPlayerReady(event): void {
    event.target.playVideo();
  }
}
