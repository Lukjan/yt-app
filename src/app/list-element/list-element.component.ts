import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Video } from '../models/video.model';

@Component({
  selector: 'app-list-element',
  templateUrl: './list-element.component.html',
  styleUrls: ['./list-element.component.scss']
})
export class ListElementComponent implements OnInit {
  @Input() video: Video;
  @HostListener('click') onClick(): void {
    this.router.navigate(['player'], {
      queryParams: {
        id: this.video.id.videoId
      }
    });
  }
  constructor(private router: Router) { }

  ngOnInit() {

  }
}
