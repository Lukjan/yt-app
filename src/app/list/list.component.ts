import { Video } from './../models/video.model';
import { YoutubeApiService } from '../services/youtube-api.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  items: Video[];
  nextPageToken: string;

  constructor(
    private route: ActivatedRoute,
    private youtubeApiService: YoutubeApiService
  ) {
    this.route.queryParams.subscribe((params => {
      if (params.q) {
        this.youtubeApiService.getSearchListByQuery(params.q).subscribe((searchedVideos => {
          this.items = searchedVideos.items;
          this.nextPageToken = searchedVideos.nextPageToken;
        }));
      }
    }));
   }

  ngOnInit() {

  }

  onScroll(): void {
    this.youtubeApiService.getSearchListNextPage(this.nextPageToken).subscribe(
      nextPageVideos => {
        this.items = [...this.items, ...nextPageVideos.items];
        this.nextPageToken = nextPageVideos.nextPageToken;
      }
    );
  }

}
