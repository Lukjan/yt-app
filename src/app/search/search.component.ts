import { MatAutocompleteTrigger, MatAutocomplete } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchHistoryQuery } from '../models/search-history-query.model';
import { startWith, map } from 'rxjs/operators';
import { SearchHistoryService } from '../services/search-history.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @ViewChild('trigger', { read: MatAutocompleteTrigger, static: false }) trigger: MatAutocompleteTrigger;
  @ViewChild('auto', { read: MatAutocomplete, static: false }) autocomplete: MatAutocomplete;

  searchForm: FormGroup;
  filteredSearchHistory$: Observable<SearchHistoryQuery[]>;
  private searchHistory: SearchHistoryQuery[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchHistoryService: SearchHistoryService
) {
    this.searchForm = this.generateSearchForm();
    this.route.queryParams.subscribe((params => {
      if (params.q) {
        this.searchForm.patchValue({search: params.q});
      }
    }));
    this.searchHistoryService.searchHistory.subscribe(searchHistory => this.searchHistory = searchHistory);
  }

  ngOnInit(): void {
    this.filteredSearchHistory$ = this.searchForm.controls.search.valueChanges
    .pipe(
      startWith(''),
      map(value => this.filter(value))
    );
  }

  onSearch(): void {
    if (this.searchForm.valid) {
      this.searchVideo(this.searchForm.value.search);
    }
  }

  clearSearchInput(): void {
    this.searchForm.controls.search.patchValue('');
    setTimeout(() => this.trigger.openPanel(), 1);
  }

  searchVideo(searchQuery: string): void {
    const newSearchQuery = {
      value: searchQuery,
      searchedAt: Date.now()
    };
    this.searchHistoryService.addSearchQueryToHistory(newSearchQuery);
    this.router.navigate(['search'], {
      queryParams: {
        q: searchQuery
      }
    });
  }

  private generateSearchForm(querry?): FormGroup {
    return new FormGroup({
      search: new FormControl(querry ? querry : '')
    });
  }

  private filter(value: string): SearchHistoryQuery[] {
    return this.searchHistory.filter(() => value.length === 0);
  }
}
