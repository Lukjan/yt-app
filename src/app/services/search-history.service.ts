import { BehaviorSubject, Observable } from 'rxjs';
import { SearchHistoryQuery } from '../models/search-history-query.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchHistoryService {
  searchHistory: Observable<SearchHistoryQuery[]>;
  private searchHistorySource$: BehaviorSubject<SearchHistoryQuery[]>;
  constructor() {
    this.searchHistorySource$ = new BehaviorSubject(this.getSearchHistory());
    this.searchHistory = this.searchHistorySource$.asObservable();
  }

  public getSearchHistory() {
    const searchHistory = JSON.parse(localStorage.getItem('searchHistory'));
    return searchHistory ? searchHistory : [];
  }

  public addSearchQueryToHistory(searchQuery: SearchHistoryQuery): void {
    searchQuery.value = searchQuery.value.toLowerCase();
    let searchHistory: SearchHistoryQuery[] = JSON.parse(localStorage.getItem('searchHistory'));
    if (searchHistory) {
      if (!this.isSearchQueryInSearchHistory(searchHistory, searchQuery)) {
        searchHistory = [searchQuery, ...searchHistory];
      }
    } else {
      searchHistory = [searchQuery];
    }
    this.searchHistorySource$.next(searchHistory);
    localStorage.setItem('searchHistory', JSON.stringify(searchHistory));
  }

  private isSearchQueryInSearchHistory(searchHistory: SearchHistoryQuery[], searchQuery: SearchHistoryQuery) {
    return !!searchHistory.find(searchHistoryElement => searchHistoryElement.value === searchQuery.value);
  }
}
