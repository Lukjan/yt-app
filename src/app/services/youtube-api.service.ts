import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { SearchList } from '../models/search-list.model';
declare global {
  interface Window {
    onYouTubeIframeAPIReady: () => void;
  }
}

@Injectable({
  providedIn: 'root'
})
export class YoutubeApiService {
  isYoutubePlayerLoaded$: Observable<boolean>;
  private isYoutubePlayerLoadedSource$: BehaviorSubject<boolean>;
  private baseSearchParams: SearchParams = {
    part: 'snippet',
    key: 'AIzaSyDWp-qcDvpFeUk5L2LEAjY9M3Lp1AfuzXA',
    type: 'video',
    maxResults: '10'
  };

  constructor(private http: HttpClient) {
    this.isYoutubePlayerLoadedSource$ = new BehaviorSubject(false);
    this.isYoutubePlayerLoaded$ = this.isYoutubePlayerLoadedSource$.asObservable();
    this.loadYoutubePlayerScript();
  }

  public getSearchListByQuery(searchString: string): Observable<SearchList> {
    return this.http.get<SearchList>('https://www.googleapis.com/youtube/v3/search', {
      params: Object.assign({q: searchString}, this.baseSearchParams)
    });
  }

  public getSearchListNextPage(pageToken: string): Observable<any> {
    return this.http.get<SearchList>('https://www.googleapis.com/youtube/v3/search', {
      params: Object.assign({pageToken}, this.baseSearchParams)
    });
  }

  private loadYoutubePlayerScript() {
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    window.onYouTubeIframeAPIReady = () => {
      this.isYoutubePlayerLoadedSource$.next(true);
    };
  }

}

interface SearchParams {
  part: string;
  key: string;
  type: string;
  maxResults: string;
  q?: string;
  pageToken?: string;
}
