export interface Video {
    etag: string;
    id: VideoId;
    kind: string;
    snippet: VideoSnippet;
}

export interface VideoId {
    kind: string;
    videoId: string;
}

export interface VideoSnippet {
    description: string;
    title: string;
    channelTitle: string;
    channelId: string;
    publishedAt: string;
    liveBroadcastContent: string;
    thumbnails: VideoThumbnails;
}

interface VideoThumbnails {
    default: Thumbnail;
    high: Thumbnail;
    medium: Thumbnail;
}

interface Thumbnail {
    url: string;
}
