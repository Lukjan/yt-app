import { PageInfo } from './page-info.model';
import { Video } from './video.model';

export interface SearchList {
    etag: string;
    kind: string;
    nextPageToken: string;
    regionCode: string;
    pageInfo: PageInfo;
    items: Video[];
}
