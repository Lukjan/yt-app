export interface SearchHistoryQuery {
    value: string;
    searchedAt: number;
}
